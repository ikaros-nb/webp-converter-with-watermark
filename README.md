# compression

## Utilisation

Ouvrir `compression.py` et modifier selon ses désirs :
- `MAXIMUM_IMAGE_SIZE`: La taille maximale de l'image (width ou height).
- `FONT_NAME`: La police à mettre dans le dossier `fonts`.
- `WATERMARK_TEXT`: Le texte à intégrer.
- `CONVERTED_IMAGES_FOLDER`: Le dossier vers lequel les images à convertir iront.

## À améliorer

### Rendre dynamique la position du watermark.

En cas de modification de `MAXIMUM_IMAGE_SIZE` ou si l'image a une taille inférieure à cette dernière, le watermark ne sera pas bien positionné.

    x = width - 1000
    y = height - 250
    
### Rendre dynamique la taille de la police selon un ratio via la taille de l'image

Actuellement, la taille de la police est fixe à 150.

    font = ImageFont.truetype(FONT_NAME, 150)

### Changer la teinte en fonction du background, pour plus de lisibilité.

Actuellement, la couleur de remplissage est la suivante :

    fill_color = (203, 201, 201)
