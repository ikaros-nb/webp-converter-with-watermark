import os
from PIL import Image, ImageDraw, ImageFont

MAXIMUM_IMAGE_SIZE = 3765
FONT_NAME = "fonts/BeckyTahlia-MP6r.ttf"
WATERMARK_TEXT = "Nicolas Boueme"
CONVERTED_IMAGES_FOLDER = "converted"


# Defining a Python user-defined exception
class Error(Exception):
    """Base class for other exceptions"""


def resize_image(picture):
    width, height = picture.size

    if width > MAXIMUM_IMAGE_SIZE or height > MAXIMUM_IMAGE_SIZE:
        picture.thumbnail(size=((MAXIMUM_IMAGE_SIZE, MAXIMUM_IMAGE_SIZE)))


def add_watermark(picture):
    # Get image size and drawing context
    width, height = picture.size
    drawing = ImageDraw.Draw(picture)

    # Define the watermark font, color and text
    font = ImageFont.truetype(FONT_NAME, 150)
    fill_color = (203, 201, 201)

    # Define the watermark text position
    x = width - 1000
    y = height - 250
    position = (x, y)

    # Write the text on the image
    drawing.text(xy=position, text=WATERMARK_TEXT, font=font, fill=fill_color)


def convert_image(image_path, image_type):
    picture = Image.open(image_path)
    picture = picture.convert('RGB')
    resize_image(picture)
    add_watermark(picture)

    if image_type in ('jpg', 'png'):
        # Spliting the image path to avoid the extension being part of the name
        new_image_name = image_path.split('.')[0]
        new_image_path = f"{CONVERTED_IMAGES_FOLDER}/{new_image_name}.webp"
        picture.save(new_image_path, 'webp', optimize=True, quality=85)
    else:
        raise Error


def is_what_percent_of(num_a, num_b):
    try:
        return round((num_a / num_b) * 100)
    except ZeroDivisionError:
        return 0


def progress_bar(percent=0, width=40):
    left = width * percent // 100
    right = width - left

    tags = "#" * left
    spaces = " " * right
    percents = f"{percent:.0f}%"

    print("\r[", tags, spaces, "] ", percents, sep="", end="", flush=True)


print(f"Create {CONVERTED_IMAGES_FOLDER} folder.")
try:
    os.mkdir(CONVERTED_IMAGES_FOLDER)
except OSError:
    print(f"The folder '{CONVERTED_IMAGES_FOLDER}' already exist.")

files = os.listdir()
images = [file for file in files if file.endswith(('jpg', 'png'))]

images_count = len(images)

if images_count > 0:
    print(f"\n{images_count} images found.\n")
    progress_bar()
else:
    print("\nThere isn't any image to convert, bitch.")

for indice, image in enumerate(images):
    percent = is_what_percent_of(indice + 1, images_count)
    if image.endswith('jpg'):
        convert_image(image, image_type='jpg')
        progress_bar(percent)
    elif image.endswith('png'):
        convert_image(image, image_type='png')
        progress_bar(percent)
    else:
        raise Error

string_matches = ['.jpg', '.png']
for file in files:
    if any(x in file for x in string_matches):
        os.remove(file)
